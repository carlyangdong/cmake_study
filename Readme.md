# cmake study project



## git module cmd

```
在项目中添加子模块命令:
git submodule add [address]

删除子模块：
submodule的删除稍微麻烦点：首先，要在“.gitmodules”文件中删除相应配置信息。然后，执行“git rm –cached ”命令将子模块所在的文件从git中删除。

子项目一起clone下来:
git clone --recursive [address]

子项目初始化：
当使用git clone下来的工程中带有submodule时，初始的时候，submodule的内容并不会自动下载下来的，此时，只需执行如下命令：
git submodule update --init --recursive

子项目更新：
git submodule foreach --recursive git checkout master
git submodule foreach git pull

```
